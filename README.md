# Performance Example

> Basic performance measurement techniques examples

## Setup

```bash
pipenv install
pipenv shell

jupyter notebook
> Open `performance_measurement_examples.ipynb`
```

## Generating HTML output

`jupyter nbconvert --to=html --output-dir=public --output=index performance_measurement_examples.ipynb`
